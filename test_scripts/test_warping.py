import numpy as np
import torch
import skimage
import matplotlib.pyplot as plt
import torchvision.transforms as transforms

# image has shape (H, W)
from plotting_utils import vis_grid
from velocity_fields import transformation
from velocity_fields_pytorch import LocalTranslation

image = skimage.color.rgb2gray(skimage.data.chelsea())

x = np.linspace(-1, 1, image.shape[1]) # use W for x
y = np.linspace(-1, 1, image.shape[0]) # use H for y
xv, yv = np.meshgrid(x, y, indexing='xy')
xy = np.stack([xv, yv], axis=2)
num_x, num_y = xv.shape

# arguments for affine_grid: size in (N, C, H, W) (target shape)
grid_torch = torch.nn.functional.affine_grid(torch.tensor([[[1, 0, 0], [0, 1, 0]]], dtype=torch.float),
                                             size=(1, 1, image.shape[0], image.shape[1]))

xy_torch = grid_torch.detach().numpy().squeeze()

diff = np.abs(xy_torch - xy)

print(np.max(diff))

v_translation = LocalTranslation(initial_direction=[(0.5, 0)], initial_center=[(-0.45, 0.25)],
                                    scale=0.1)

image_flipped = np.flip(image, axis=0)
fig = plt.figure()
ax = fig.gca()
ax.pcolormesh(xv, yv, image_flipped, cmap='gray')
v_translation.plot(fig)
fig.show()

xy_flat_torch = torch.tensor(xy.reshape(-1, 2))
displacements = v_translation(xy_flat_torch)
result = xy_flat_torch + displacements
result = result.view(xy.shape)
result = result.unsqueeze(0)

fig = vis_grid(result.detach().numpy().reshape(xy.shape), subsample=10)
v_translation.plot(fig)
fig.show()



image_flipped_torch = transforms.ToTensor()(image_flipped.copy())
warped_image = torch.nn.functional.grid_sample(image_flipped_torch.unsqueeze(0), result, mode='bilinear', align_corners=False)
warped_image_np = warped_image.detach().numpy().squeeze()


plt.pcolormesh(xv, yv, warped_image_np, cmap='gray')
plt.show()