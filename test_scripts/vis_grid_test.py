from plotting_utils import vis_grid_displacements, vis_arrows, vis_grid
import numpy as np
from velocity_fields import sum_of_translations, sum_of_scalings, sum_of_rotations, transformation

x = np.linspace(-1, 1, 10)
xv, yv = np.meshgrid(x, x, indexing='ij')
xy = np.stack([xv, yv], axis=2)

displacements = np.zeros_like(xy)
# displacement_x = np.exp(-(xv)**2)*np.exp(-(x+1)**2/4)
# displacement_y = np.zeros_like(yv)
# displacements = np.stack([displacement_x, displacement_y], axis=2)

v_translation = sum_of_translations(directions=[(1, 1), (-1, -1)], centers=[(0.5, 0.5), (-0.5, -0.5)],
                                    controls=[0.1, 0.1], sigma=0.5)
fig = vis_grid_displacements(displacements, subsample=2)
fig1 = vis_grid_displacements(v_translation(xv, yv), color='blue')
fig1.show()
fig2 = vis_arrows(v_translation(xv, yv), fig=None, xv=xv, yv=yv, subsample=1)
fig2.show()

v_scaling = sum_of_scalings(centers=[(0, 0), (0.5, 0.5)], controls=[1.5, 1.5], sigma=0.5)
# fig1 = vis_grid_displacements(v_scaling(xv, yv), xv=xv, yv=yv)
# #fig1 = vis_grid_displacements(displacements, fig=fig1, xv=xv, yv=yv, color='blue')
# fig1.show()
# fig2 = vis_arrows(v_scaling(xv, yv), xv=xv, yv=yv)
# fig2.show()

v_rotation = sum_of_rotations(centers=[(-0.5, -0.5), (0.5, 0.5)], controls=[0.5, -0.5], sigma=0.1)
v_rotation = sum_of_rotations(centers=[(0, 0)], controls=[0.5], sigma=4)
fig1 = vis_grid_displacements(v_rotation(xv, yv), xv=xv, yv=yv)
# fig1 = vis_grid_displacements(displacements, fig=fig1, xv=xv, yv=yv, color='blue')
# fig1.show()
# fig2 = vis_arrows(v_rotation(xv, yv), xv=xv, yv=yv)
# #fig2.show()
# transf_rot = transformation(xy, v_rotation)
# transf_rot_2 = transformation(transf_rot, v_rotation)
# transf_rot_3 = transformation(transf_rot_2, v_rotation)
#
# fig4 = vis_grid(transf_rot_2)
# fig4.show()
#
# fig5 = vis_grid(transf_rot_3)
# fig5.show()