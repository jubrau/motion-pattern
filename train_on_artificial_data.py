import torch
import torch.optim as optim
import matplotlib.pyplot as plt
from itertools import cycle
from torch.utils.tensorboard import SummaryWriter
import seaborn as sns

from local_diffeomorphism_networks import MultiScaleNetwork
import numpy as np

from plotting_utils import vis_grid, vis_grid_displacements

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# define neural network to train
from training_utils import plot_grid, plot_loss_on_grid, plot_grid_two_scales

config_dict_1 = [{'type': 'scaling'}, {'type': 'rotation'}, {'type': 'translation'}] * 2
config_dict_2 = config_dict_1 * 2
config_dict_translation = [{'type': 'translation'}] * 2
config_dict_translation_2 = config_dict_translation * 2


scales = [1, 0.5]
train_network = MultiScaleNetwork(1, config_dicts=[config_dict_1, config_dict_2], scales=scales)
train_network.to(device)

# define input data
x = np.linspace(-1, 1, 50)
xv, yv = np.meshgrid(x, x, indexing='ij')
xy = np.stack([xv, yv], axis=2)
xy_flat = xy.reshape(-1, 2)
xy_flat_tensor = torch.tensor(xy_flat, dtype=torch.float)

train_network.initialize_random_centers_(xy_flat)


# define neural network to generate data
config_dict_1_gen = [{'type': 'rotation', 'params': {'initial_center': (0, 0)}, 'alpha': 1}]
config_dict_2_gen = [{'type': 'rotation', 'params': {'initial_center': (-0.5, -0.5)}, 'alpha': -1},
                     {'type': 'rotation', 'params': {'initial_center': (0.5, 0.5)}, 'alpha': -1}]

# define train network by disturbing generate network
train_network.velocity_fields[0].local_velocity_fields[1].set_center_([0, 0])
train_network.velocity_fields[0].set_alphas_([0, 1, 0, 0, 0, 0])
train_network.velocity_fields[1].local_velocity_fields[1].set_center_([0.5, 0.5])
train_network.velocity_fields[1].local_velocity_fields[4].set_center_([-0.5, -0.5])
train_network.velocity_fields[1].set_alphas_([0, -1, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0])
print(train_network)
# train_network.disturb_(sigma_alphas=0, sigma_centers=0.1)
train_network.to(device)
print(train_network)

scales_gen = scales
generate_network = MultiScaleNetwork(1, config_dicts=[config_dict_1_gen, config_dict_2_gen], scales=scales_gen)

outs = generate_network.calculate_all_scales(xy_flat_tensor)
outs_numpy = [out.detach().numpy() for out in outs]
labels = torch.tensor(outs_numpy[1], dtype=torch.float).to(device)

color_palette = sns.color_palette('deep')
fig3 = vis_grid(outs_numpy[1].reshape(xy.shape), xv=xv, yv=yv, subsample=5, zorder=0, color=color_palette[0])
fig3 = vis_grid_displacements(np.zeros_like(xy), fig=fig3, color='grey', subsample=5, alpha=0.3, zorder=0)
fig3 = generate_network.plot(fig3, color_palette=color_palette[1:])
fig3.show()
# fig = vis_grid(outs[0].reshape(xy.shape), xv=xv, yv=yv)
# fig.show()
# fig2 = vis_grid(outs[1].reshape(xy.shape), xv=xv, yv=yv, fig=fig, color='blue')
# fig2.show()

loss_mean = torch.nn.MSELoss(reduction='mean')

lambda_redundancy = 0
lambda_sparsity = 1e-3

# remove influence of small scale
train_network.velocity_fields[1].set_alphas_(0)
# train only large scale
optimizer1 = optim.Adam(train_network.velocity_fields[0].parameters(), lr=0.01)
optimizer2 = optim.Adam(train_network.velocity_fields[1].parameters(), lr=0.01)
optimizer = optim.Adam(train_network.parameters(), lr=0.1)
reference_loss = np.mean((outs_numpy[1] - outs_numpy[0])**2)
optimizers = cycle([optimizer1, optimizer2])
# optimizers = cycle([optimizer1])

save_path = '/u/j_brau09/PycharmProjects/motion-patterns/runs/disturb/no_disturb_l1_1e-3'
writer = SummaryWriter(save_path)
max_loss = None

xy_flat_tensor = xy_flat_tensor.to(device)

for epoch in range(10000):

    # switch which parameters are optimized every 500 epochs
    # if epoch % 2000 == 0:
        # optimizer = next(optimizers)

    # zero the parameter gradients
    # optimizer.zero_grad()

    # forward + backward + optimize
    # loss = criterion(outputs, labels)
    # loss.backward()

    def calculate_losses(outputs):
        loss = loss_mean(outputs, labels)
        loss_redundancy = train_network.redundancy_loss()
        loss_sparsity = train_network.regularization_loss(1)
        loss_total = loss + lambda_redundancy * loss_redundancy + lambda_sparsity * loss_sparsity
        return loss, loss_sparsity, loss_redundancy, loss_total

    def closure():
        # zero the parameter gradients
        optimizer.zero_grad()
        outputs = train_network(xy_flat_tensor)
        loss_total = calculate_losses(outputs)[-1]
        loss_total.backward()
        return loss_total


    optimizer.step(closure)

    with torch.no_grad():
        outputs = train_network(xy_flat_tensor)
        loss, loss_sparsity, loss_redundancy, loss_total = calculate_losses(outputs)
        if epoch == 0:
            max_loss = np.max(np.sum((outputs.detach().cpu().numpy()-labels.detach().cpu().numpy())**2, axis=-1))

        # print statistics
        if epoch % 100 == 0:
            writer.add_scalar('loss', loss.item(), epoch)
            if lambda_sparsity > 0:
                writer.add_scalar('sparsity_loss', loss_sparsity.item(), epoch)
            if lambda_redundancy > 0:
                writer.add_scalar('redundancy_loss', loss_redundancy.item(), epoch)
            writer.add_figure('deformation', plot_grid(outputs, labels, xv, yv, subsample=5), global_step=epoch)
            writer.add_figure('loss on grid', plot_loss_on_grid(outputs, labels, xv, yv, max_loss=max_loss),
                              global_step=epoch)
            writer.add_figure('deformation more info',
                              plot_grid_two_scales(train_network, xy_flat_tensor, xv, yv, subsample=5),
                              global_step=epoch)
            print(f'[{epoch + 1:d}] loss: {loss.item():.3f}')

writer.close()
torch.save(train_network.state_dict(), save_path + 'state_dict.pth')

# plot
fig = plot_grid(outputs, labels, xv, yv, subsample=5)
fig.show()
fig2 = plot_loss_on_grid(outputs, labels, xv, yv)
fig2.show()
