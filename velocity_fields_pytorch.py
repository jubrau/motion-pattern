import torch
import numpy as np

from plotting_utils import get_points


class ParametrizedLocalVelocityField(torch.nn.Module):

    def __init__(self, scale=1, initial_center=(0.0, 0.0)):
        super().__init__()
        self.scale = scale
        self.center = torch.nn.Parameter(torch.tensor(initial_center, dtype=torch.float).view(1, -1))

    def forward(self, xy):
        return torch.zeros_like(xy)

    def set_random_center_(self, xy):

        center = xy[np.random.choice(len(xy))]
        self.set_center_(center)

    def set_center_(self, center):
        """
        Set the center of the velocity field to the given value.
        :param center: new center
        """
        with torch.no_grad():
            self.center.copy_(torch.tensor(center, dtype=torch.float))

    def set_trainable_(self, trainable: bool):
        """
        Make all parameters of model trainable or not.
        :param trainable: bool, trainable or non-trainable
        :return: None
        """
        for param in self.parameters():
            param.requires_grad_(trainable)

    def gaussian_kernel_func(self, xy, center=None):
        """
        Gaussian kernel function,

        .. math:: K_\sigma(xy, center) = exp(-(xy-center)²/(2\sigma ²),

        where :math:`\sigma` is defined by self.scale.

        :param xy: tensor of shape (batch_size, 2)
        :param center: center of the gaussian kernel, if None use self.center
        :return: kernel function evaluated at xy, shape (batch_size, 1)
        """
        if center is None:
            center = self.center
        return torch.exp(-torch.sum((xy-center)**2, dim=1, keepdim=True) / (2 * self.scale ** 2))

    def abstract_cost(self, centers, vectors):
        """

        :param centers: list of kernel centers
        :param vectors: arrows attached to kernel centers
        :return: cost
        """
        # check this
        cost = 0
        for i in range(len(centers)):
            for j in range(0, len(centers)):
                cost += self.gaussian_kernel_func(centers[i], centers[j]) * torch.sum(vectors[i] * vectors[j], dim=-1,
                                                                                      keepdim=True)
        return cost

    def plot(self, fig=None, s=0, color=None):
        if fig is None:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            return fig
        else:
            return fig

    def __repr__(self):
        return f'{self.__class__.__name__}({np.array2string(self.center.detach().cpu().numpy().squeeze(), precision=2)})'

    def disturb_(self, sigma):
        """
        Add zero mean gaussian noise to center
        :param sigma: standard deviation of noise
        """
        noise = np.random.normal(scale=sigma, size=self.center.shape)
        old_center = self.center.detach().cpu().numpy()
        self.set_center_(old_center + noise)


class LocalTranslation(ParametrizedLocalVelocityField):
    """
    Vector field representing translation, v(·)= K_scale(·, center) * direction
    """

    def __init__(self, scale=1, initial_center=(0, 0), initial_direction=(1, 0), initial_angle=0):
        """
        :param scale: scaling factor used in gaussian kernel
        :param initial_center:  tuple or array with two elements, x and y coordinate
        :param initial_direction:  tuple or array with two elements, x and y coordinate
        """
        super().__init__(scale, initial_center)
        self.direction = torch.nn.Parameter(torch.tensor(initial_direction, dtype=torch.float).view(1, -1))
        # self.angle = torch.nn.Parameter(torch.tensor(initial_angle, dtype=torch.float).view(1, 1))
        # self.direction = torch.stack([torch.cos(self.angle), torch.sin(self.angle)]).view(1, 2)
        # todo: should the direction be normed? e.g. different parametrization? or should the controls be in this class
        # on the other hand: overparametrized anyway, and overparametrization is good

    def forward(self, xy):
        """
        :param xy: shape (batch_size, 2)
        :return: result shape (batch_size, 2)
        """
        result = self.gaussian_kernel_func(xy) * self.direction
        return result

    def plot(self, fig=None, s=0, color='b'):
        """
        Plot marker on axes.
        :param fig: figure
        :param s: scaling factor for arrow, in coordinates unit
        :param color: color
        :return: axes
        """
        fig = super().plot(fig, s=s, color=color)
        ax = fig.gca()
        center = self.center.detach().cpu().numpy().squeeze()
        direction = self.direction.detach().cpu().numpy().squeeze()
        if s != 0:
            ax.arrow(center[0], center[1], s*direction[0], s*direction[1], color=color, head_width=0.05, head_length=0.05,
                     length_includes_head=True, fill=True, overhang=0)
            ax.scatter(center[0], center[1], marker='X', color=color, s=get_points(fig)/20)

    def cost(self, alpha):
        return alpha ** 2


class LocalScaling(ParametrizedLocalVelocityField):
    """Velocity field representing local scaling at center on specified scale. """

    def __init__(self, scale=1, initial_center=(0, 0)):
        """
        :param scale: scaling factor used in gaussian kernel
        :param initial_center: tuple or array with two elements, x and y coordinate
        """
        super().__init__(scale, initial_center)
        # some constants
        d1 = torch.tensor(np.array([[0.5 * np.sqrt(3), -0.5]]), dtype=torch.float)
        d2 = torch.tensor(np.array([[-0.5 * np.sqrt(3), -0.5]]), dtype=torch.float)
        d3 = torch.tensor(np.array([[0, 1]]), dtype=torch.float)
        self.d1 = torch.nn.Parameter(d1, requires_grad=False)
        self.d2 = torch.nn.Parameter(d2, requires_grad=False)
        self.d3 = torch.nn.Parameter(d3, requires_grad=False)

    def forward(self, xy):
        """
        :param xy: shape (batch_size, 2)
        :return: result shape (batch_size, 2)
        """
        # shorten notation
        # d1, d2, d3 = LocalScaling.d1, LocalScaling.d2, LocalScaling.d3
        d1, d2, d3 = self.d1, self.d2, self.d3
        # calculate result
        result = self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * d1) * d1 \
            + self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * d2) * d2 \
            + self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * d3) * d3
        return result

    def plot(self, fig=None, s=0, color='g'):
        """
        Plot marker on axes.
        :param fig: figure
        :param s: scaling factor for marker
        :param color: color
        :return: axes
        """
        fig = super().plot(fig, s=s, color=color)
        ax = fig.gca()
        center = self.center.detach().cpu().numpy().squeeze()
        if s > 0:
            marker = u'$\u2b25$'  # ⬥
        elif s < 0:
            marker = u'$\u27e1$'  # ⟡

        if s != 0:
            ax.scatter(center[0], center[1], marker=marker, s=get_points(fig)*abs(s), color=color, linewidth=0.1)

    def cost(self, alpha):
        # TODO
        return 0


class LocalRotation(ParametrizedLocalVelocityField):
    """Velocity field representing local scaling at center on specified scale. """

    def __init__(self, scale=1, initial_center=(0, 0)):
        """
        :param scale: scaling factor used in gaussian kernel
        :param initial_center:  tuple or array with two elements, x and y coordinate
        """
        super().__init__(scale, initial_center)
        # some constants. all of shape (1,2)
        self.d1 = torch.nn.Parameter(torch.tensor(np.array([[0.5, 0.5 * np.sqrt(3)]]), dtype=torch.float),
                                     requires_grad=False)
        self.d2 = torch.nn.Parameter(torch.tensor(np.array([[0.5, -0.5 * np.sqrt(3)]]), dtype=torch.float),
                                     requires_grad=False)
        self.d3 = torch.nn.Parameter(torch.tensor(np.array([[-1, 0]]), dtype=torch.float),
                                     requires_grad=False)
        self.z1 = torch.nn.Parameter(torch.tensor(np.array([[0.5 * np.sqrt(3), -0.5]]), dtype=torch.float),
                                     requires_grad=False)
        self.z2 = torch.nn.Parameter(torch.tensor(np.array([[-0.5 * np.sqrt(3), -0.5]]), dtype=torch.float),
                                     requires_grad=False)
        self.z3 = torch.nn.Parameter(torch.tensor(np.array([[0, 1]]), dtype=torch.float),
                                     requires_grad=False)

        self.cost_constant = self.abstract_cost(
            [self.scale / 3 * self.z1, self.scale / 3 * self.z2, self.scale / 3 * self.z3], [self.d1, self.d2, self.d3])

    def forward(self, xy):
        """
        :param xy: shape (batch_size, 2)
        :return: result shape (batch_size, 2)
        """
        # calculate result
        result = self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * self.z1) * self.d1 \
            + self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * self.z2) * self.d2 \
            + self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * self.z3) * self.d3
        return result

    def plot(self, fig=None, s=0, color='r'):
        """
        Plot marker on axes.
        :param fig: figure
        :param s: scaling factor for marker
        :param color: color
        :return: axes
        """
        fig = super().plot(fig, s=s, color=color)
        ax = fig.gca()
        center = self.center.detach().cpu().numpy().squeeze()
        if s > 0:
            marker = r'$\circlearrowleft$'  # ↺
        elif s < 0:
            marker = r'$\circlearrowright$'  # ↻

        if s != 0:
            ax.scatter(center[0], center[1], marker=marker, color=color, s=get_points(fig)*abs(s), linewidth=0.1)

    def cost(self, alpha):
        return alpha ** 2 * self.cost_constant


class LocalAnisotropicScaling(ParametrizedLocalVelocityField):

    def __init__(self, scale, initial_center, initial_alphas=(1, 1)):
        super().__init__(scale, initial_center)
        d = self.get_arrow_vectors()
        self.d1 = torch.nn.Parameter(torch.tensor(d[0], dtype=torch.float,
                                                  requires_grad=False))
        self.d2 = torch.nn.Parameter(torch.tensor(d[1], dtype=torch.float,
                                                  requires_grad=False))
        self.d3 = torch.nn.Parameter(torch.tensor(d[2], dtype=torch.float,
                                                  requires_grad=False))
        self.z1 = torch.nn.Parameter(torch.tensor(np.array([[0.5 * np.sqrt(3), -0.5]]), dtype=torch.float),
                                     requires_grad=False)
        self.z2 = torch.nn.Parameter(torch.tensor(np.array([[-0.5 * np.sqrt(3), -0.5]]), dtype=torch.float),
                                     requires_grad=False)
        self.z3 = torch.nn.Parameter(torch.tensor(np.array([[0, 1]]), dtype=torch.float),
                                     requires_grad=False)
        self.alphas = torch.nn.Parameter(torch.tensor(initial_alphas, dtype=torch.float).view(1, -1))

    def vector_from_alpha(self, alpha):
        # alpha has shape (1, 2)
        alpha_temp = (alpha - 1) / alpha
        return self.d1 * alpha_temp, self.d2 * alpha_temp, self.d3 * alpha_temp

    def get_arrow_vectors(self):
        d1 = np.array([[0.5 * np.sqrt(3), -0.5]])
        d2 = np.array([[-0.5 * np.sqrt(3), -0.5]])
        d3 = np.array([[0, 1]])
        k = np.exp(-3 / (2 * self.scale ** 2))
        factor = 1 / (2 * k ** 2 - k - 1)
        matrix = factor * np.array([[-k - 1, k, k], [k, -k - 1, k], [k, k, -k - 1]])
        d = np.concatenate([d1, d2, d3])
        d_new = np.matmul(matrix, d) * self.scale / 3
        return d_new.reshape(3, 1, 2)

    def forward(self, xy):
        """
        :param xy: shape (batch_size, 2)
        :return: result shape (batch_size, 2)
        """
        # calculate result
        d1, d2, d3 = self.vector_from_alpha(self.alphas)
        result = self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * self.z1) * d1 \
                 + self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * self.z2) * d2 \
                 + self.gaussian_kernel_func(xy, center=self.center + self.scale / 3 * self.z3) * d3
        return result

    def cost(self, alpha):
        d1, d2, d3 = self.vector_from_alpha(self.alphas)
        return alpha ** 2 * self.abstract_cost([self.z1, self.z2, self.z3], [d1, d2, d3])

    def plot(self, fig=None, s=0, color='g'):
        """
        Plot marker on axes.
        :param fig: figure
        :param s: scaling factor for marker
        :param color: color
        :return: axes
        """
        fig = super().plot(fig, s=s, color=color)
        ax = fig.gca()
        center = self.center.detach().cpu().numpy().squeeze()
        if s != 0:
            ax.scatter(center[0], center[1], marker='d', s=get_points(fig) * abs(s), color=color, linewidth=0.1)
