import torch

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.figure import figaspect

from plotting_utils import vis_grid, vis_grid_displacements
import seaborn as sns


def plot_true_and_transformed(module, input_image, output, target, xv, yv):
    output = output.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1])
    target = target.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1])
    input_image = input_image.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1])
    # use seaborn style
    sns.set_style("white")
    w, h = figaspect(1. / 4)
    fig = plt.figure(figsize=(w, h))
    # first plot
    ax4 = fig.add_subplot(1, 4, 1)
    plt.imshow(input_image, origin='lower')
    ax4.set_title('Input')
    ax1 = fig.add_subplot(1, 4, 2)
    plt.imshow(output, vmin=0, vmax=1)
    ax1.set_title('Output')
    # plot markers
    current_palette = sns.color_palette(n_colors=2)
    # module.plot(fig, color_palette=current_palette[1:])
    ax2 = fig.add_subplot(1, 4, 3)
    plt.imshow(target, vmin=0, vmax=1)
    ax2.set_title('Target')
    # plot difference
    ax3 = fig.add_subplot(1, 4, 4)
    plt.imshow(np.abs(target - output), vmin=0, vmax=1)
    # plt.legend()
    ax3.set_title('L1 difference')
    return fig


def plot_grid(outputs, labels, xv, yv, subsample=1):
    """
    Display true deformation, learned deformation and difference in a row.
    :param outputs: Tensor, output of net
    :param labels: Tensor, true deformation
    :param xv: xv from meshgrid
    :param yv: yv from meshgrid
    :param subsample: subsample using only every "subsample" value of the input
    :return: generated matplotlib figure
    """
    # let's take it all to numpy for convenience
    outputs = outputs.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1], 2)
    labels = labels.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1], 2)
    # use seaborn style
    sns.set_style("white")
    current_palette = sns.color_palette(n_colors=2)
    # https://stackoverflow.com/questions/42973223/how-share-x-axis-of-two-subplots-after-they-are-created
    # create figure
    w, h = figaspect(1. / 3)
    fig = plt.figure(figsize=(w, h))
    # first plot
    ax1 = fig.add_subplot(1, 3, 1)
    fig = vis_grid(labels, fig=fig, xv=xv, yv=yv, color=current_palette[0], subsample=subsample)
    ax1.set_title('True')
    # second plot
    ax2 = fig.add_subplot(1, 3, 2, sharex=ax1, sharey=ax1)
    fig = vis_grid(outputs, fig=fig, xv=xv, yv=yv, color=current_palette[1], subsample=subsample)
    ax2.set_title('Learned')
    # third plot
    ax3 = fig.add_subplot(1, 3, 3, sharex=ax1, sharey=ax1)
    fig = vis_grid(outputs, fig=fig, xv=xv, yv=yv, color=current_palette[1], subsample=subsample)
    fig = vis_grid(labels, fig=fig, xv=xv, yv=yv, color=current_palette[0], subsample=subsample)
    ax3.set_title('True vs. Learned')
    return fig


def plot_loss_on_grid(outputs, labels, xv, yv, max_loss=None):
    # let's take it all to numpy for convenience
    outputs = outputs.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1], 2)
    labels = labels.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1], 2)
    diff = np.sum((outputs - labels) ** 2, axis=-1)
    diff = diff[:-1, :-1]
    #sns.set_style('white')
    # create figure
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    # https://stackoverflow.com/a/54088910
    if max_loss is None:
        vmax = diff.max()
    else:
        vmax = max_loss
    c = ax.pcolormesh(xv, yv, diff, vmin=0, vmax=vmax, cmap='YlGnBu_r')
    ax.axis([xv.min(), xv.max(), yv.min(), yv.max()])
    ax.set_aspect('equal')
    fig.colorbar(c, ax=ax)
    ax.set_title('Difference between true and learned')
    return fig


def plot_module(module, xv, yv, subsample=1):
    # define inputs
    xy = np.stack([xv, yv], axis=2)
    xy_flat = xy.reshape(-1, 2)
    xy_flat_tensor = torch.tensor(xy_flat, dtype=torch.float)
    outputs = module(xy_flat_tensor)
    # take it to numpy
    outputs = outputs.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1], 2)
    # plot using grid visualization function
    color_palette = sns.color_palette('deep')
    fig = plt.figure()
    fig = vis_grid(outputs, fig=fig, xv=xv, yv=yv, subsample=subsample, color=color_palette[0], zorder=0)
    fig = vis_grid_displacements(np.zeros_like(xy), fig=fig, xv=xv, yv=yv, subsample=subsample, zorder=0, color='grey')
    # plot markers
    module.plot(fig, color_palette=color_palette[1:])
    return fig


def plot_grid_two_scales(module, xy_flat_tensor, xv, yv, subsample=1):
    # calculate outputs
    outputs = module.calculate_all_scales(xy_flat_tensor)
    # take it to numpy
    outputs = [output.detach().cpu().numpy().reshape(xv.shape[0], xv.shape[1], 2) for output in outputs]
    # plot using grid visualization function
    color_palette = sns.color_palette('deep')
    # create figure
    w, h = figaspect(1. / 3)
    fig = plt.figure(figsize=(w, h))
    # first plot
    ax1 = fig.add_subplot(1, 3, 1)
    fig = vis_grid(outputs[0], fig=fig, xv=xv, yv=yv, subsample=subsample, color=color_palette[0])
    #fig = vis_grid_displacements(np.zeros_like(xy), fig=fig, xv=xv, yv=yv, subsample=subsample, zorder=0,
    #                             color='grey')
    ax1.set_title('Largest scale')
    # second plot
    ax2 = fig.add_subplot(1, 3, 2, sharex=ax1, sharey=ax1)
    fig = vis_grid(outputs[1], fig=fig, xv=xv, yv=yv, subsample=subsample, color=color_palette[0])
    #fig = vis_grid_displacements(np.zeros_like(xy), fig=fig, xv=xv, yv=yv, subsample=subsample, zorder=0,
    #                             color='grey')
    ax2.set_title('Second largest scale')
    # plot markers
    ax3 = fig.add_subplot(1, 3, 3)
    fig = vis_grid(outputs[-1], fig=fig, xv=xv, yv=yv, subsample=subsample, color=color_palette[0], zorder=0)
    #fig = vis_grid_displacements(np.zeros_like(xy), fig=fig, xv=xv, yv=yv, subsample=subsample, zorder=0,
    #                             color='grey')
    fig = module.plot(fig, color_palette=color_palette[1:])
    ax3.set_title('Final deformation with markers')
    return fig

